var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var index = require('./routes/index');
var users = require('./routes/users');
var todos = require('./routes/todos');
var counter = require('./routes/counter');
var projects = require('./routes/projects');

const DATABASE_URL = 'mongodb://localhost:27017/'
const DATABASE = 'Testing-API'

const SERVER_IP = 'http://localhost:4000';

var app = express();

// COORS Headers
// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', SERVER_IP);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/todos', todos);
app.use('/counter', counter)
app.use('/projects', projects)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Use native Node promises
mongoose.Promise = global.Promise;
// connect to MongoDB
mongoose.connect(DATABASE_URL + DATABASE)
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));

module.exports = app;
