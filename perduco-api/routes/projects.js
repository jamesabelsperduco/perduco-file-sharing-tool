var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var Projects = require('../models/Projects.js');

/* GET /project listing. */
router.get('/', function(req, res, next) {
  Projects.find(function (err, projects) {
    if (err) return next(err);
    res.json(
      {
        time_requested: Date.now(),
        length: projects.length,
        data: projects
      }
    );
  });
});

/* POST /project */
router.post('/', function(req, res, next) {
  Projects.create(req.body, function (err, post) {
    if (err) return next(err);
    var parsedPost = post;

    parsedPost.time_uploaded = Date.now()

    res.json(parsedPost);
  });
});

/* DELETE /project /:id */
router.delete('/:id', function(req, res, next) {
  Projects.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;