var mongoose = require('mongoose');

var ProjectSchema = new mongoose.Schema({
  title: String,
  description: String,
  time_created: Date,
  files: [{
    title: String,
    description: String, 
    data: String,
    time_created: Date,
    last_modified: Date,
    version: Number,
    comments: [{
      username: String,
      time_posted: Date,
      last_modified: Date,
      comment_body: String
    }]
  }]
});

module.exports = mongoose.model('Projects', ProjectSchema);