var mongoose = require('mongoose');

var CounterSchema = new mongoose.Schema({
  current_count: Number,
});

module.exports = mongoose.model('CounterCollection', CounterSchema);