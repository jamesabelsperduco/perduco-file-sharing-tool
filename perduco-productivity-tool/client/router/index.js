import Vue from 'vue'
import Router from 'vue-router'

import Home from '../views/Home'
import ProjectView from '../views/Project-View'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/project/:id',
      component: ProjectView
    }
  ]
})
