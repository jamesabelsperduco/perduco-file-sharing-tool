import './promise-polyfill'
import {app} from './app'

// import * as $ from 'jQuery'
import './assets/bootstrap/css/bootstrap.min.css'
import './assets/fonts/ionicons.min.css'
import './assets/css/Projects-Clean.css'
import './assets/css/Login-Form-Clean.css'
import './assets/css/Registration-Form-with-Photo.css'
import './assets/css/Navigation-Clean1.css'
import './assets/css/Navigation-with-Button1.css'
import './assets/css/styles.css'

app.$mount('#app')
