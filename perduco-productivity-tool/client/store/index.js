import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const state = {
  count: 0,
  current_id: 0,
  projects: []
}

const mutations = {
  INCREMENT(state) {
    state.count++  
  },
  DECREMENT(state) {
    state.count--
  },
  UPDATE (state, payload) {
    state.count = payload.value
  },
  UPDATE_ID (state, payload) {
    state.current_id = payload.value
  },
  UPDATE_PROJECTS (state, payload) {
    state.projects = payload.value
  } 
}

const actions = {
  incrementAsync({commit}) {
    setTimeout(() => {
      commit('INCREMENT')
    }, 200)
  },
  updateCount ({commit}) {
    
    axios.get('http://localhost:3000/counter', {
      current_count: state.count++ 
    })
    .then(function (response) {
      console.log(response.data);

      commit('UPDATE', {value: response.data.length})

    })
    .catch(function (error) {
      console.log(error);
    });
  },
  increment ({commit}) {

    commit('INCREMENT')

    axios.post('http://localhost:3000/counter', {
      current_count: state.count
    })
    .then(function (response) {
      console.log(response.statusText);
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  getID ({commit, state}) {
    axios.get('http://localhost:3000/counter')
    .then(function (response) {
      console.log(response.data.data[response.data.data.length - 1]._id);

      var id = response.data.data[response.data.data.length - 1]._id
      
      commit('UPDATE_ID', {value: id})
      console.log('Last ID', state.current_id)
      console.log(response.data.data.length)
      
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  deincrement ({commit, state}) {

    axios.delete('http://localhost:3000/counter/' + state.current_id)
    .then(function (response) {
      console.log(response.statusText);
      commit('DECREMENT')   
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  addProject (context) {
    axios.post('http://localhost:3000/projects', {
      title: "My Project",
      description: "A rad project!",
      time_created: Date.now(),
      files: [
        {title: 'A File', description: 'This is a file', data:'thisissomebase64', time_created: '', last_modified: '', version: ''}, 
        {title: 'A File', description: 'This is a file', data:'thisissomebase64'}, 
        {title: 'A File', description: 'This is a file', data:'thisissomebase64'}, 
      ]
    })
    .then(function (response) {
      console.log(response.statusText, response);
      context.dispatch('getProjects')

    })
    .catch(function (error) {
      console.log(error);
    });
  },
  getProjects({commit}) {
    axios.get('http://localhost:3000/projects')
    .then(function (response) {
      console.log('Getting projects...')
      console.log(response.data.data)
      commit('UPDATE_PROJECTS', {value: response.data.data})
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  deleteProject (context, payload) {
    axios.delete('http://localhost:3000/projects/' + payload.value)
    .then(function (response) {
      console.log(response.statusText)
      context.dispatch('getProjects')
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
